#!/bin/bash

#SBATCH --output=../log/Train_prior_%j.out
#SBATCH --gres=gpu:2
#SBATCH --mem=60G


echo "script START"

source /usr/itetnas04/data-scratch-01/$USER/data/conda/etc/profile.d/conda.sh
conda activate liif_v4
echo "Conda Environment Activated"


start=$(date +%s)
#python -u scripts/hello.py "$@"
python train_liif.py --config ./configs/train-div2k/train_deep_image_prior_v4.yaml


end=$(date +%s)
echo "Elapsed Time: $(($end-$start)) seconds"
echo "script END"