#!/bin/bash

#SBATCH --output=../log/Test_Medical_onall_%j.out
#SBATCH --gres=gpu:1
#SBATCH --mem=30G

echo "script START"

source /usr/itetnas04/data-scratch-01/$USER/data/conda/etc/profile.d/conda.sh
conda activate liif_v4
echo "Conda Environment Activated"


start=$(date +%s)
#python -u scripts/hello.py "$@"
echo "Medical on Medical Dataset"
python -W ignore test.py --config ./configs/test/test_medical.yaml --model ./save/_train-medical/epoch-200.pth --gpu 0
echo "Medical on Div2k"
python -W ignore test.py --config ./configs/test/test-div2k.yaml --model ./save/_train-medical/epoch-200.pth --gpu 0
echo "Medical on Benchmark"
python -W ignore test.py --config ./configs/test/test-set5-2.yaml --model ./save/_train-medical/epoch-200.pth --gpu 0
echo "Medical on Places365"
python -W ignore test.py --config ./configs/test/test-Places365.yaml --model ./save/_train-medical/epoch-200.pth --gpu 0



end=$(date +%s)
echo "Elapsed Time: $(($end-$start)) seconds"
echo "script END"