#!/bin/bash

#SBATCH --output=../log/Test_all_Medical_x30_v2_%j.out
#SBATCH --gres=gpu:1
#SBATCH --mem=30G

echo "script START"

source /usr/itetnas04/data-scratch-01/$USER/data/conda/etc/profile.d/conda.sh
conda activate liif_v4
echo "Conda Environment Activated"


start=$(date +%s)
#python -u scripts/hello.py "$@"
echo "Medical on Medical"
python -W ignore test.py --config ./configs/test/test_medical_30.yaml --model ./save/_train-medical/epoch-200.pth --gpu 0
echo "Div on medical"
python -W ignore test.py --config ./configs/test/test_medical_30.yaml --model ../pretrained_models/edsr-baseline-liif.pth --gpu 0
echo "Places on Medical"
python -W ignore test.py --config ./configs/test/test_medical_30.yaml --model ./save/_train_Places365-32-256_liif/epoch-100.pth --gpu 0



end=$(date +%s)
echo "Elapsed Time: $(($end-$start)) seconds"
echo "script END"