import numpy as np
import matplotlib.pyplot as plt
from PIL import Image


# noisy_im = np.random.uniform(0,0.1,(678,1020,3))

# plt.imsave(f'./load/div2k/test/random.png', noisy_im)

img = Image.open('./load/div2k/DIV2K_valid_LR_bicubic/X4/0801x4.png')
x, y = img.size
x *= 4
y *= 4
print(x,y)
img = img.resize((x,y), resample = Image.BILINEAR)
img.save('./load/div2k/single_image/low_res/pixelated.png', format='png')