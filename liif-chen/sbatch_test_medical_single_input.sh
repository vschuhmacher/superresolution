#!/bin/bash

#SBATCH --output=../log/Test_MEDICAL_%j.out
#SBATCH --gres=gpu:1
#SBATCH --mem=30G

echo "script START"

source /usr/itetnas04/data-scratch-01/$USER/data/conda/etc/profile.d/conda.sh
conda activate liif_v4
echo "Conda Environment Activated"
#
#./../pretrained_models/edsr-baseline-liif.pth


start=$(date +%s)
#python -u scripts/hello.py "$@"
python -W ignore demo.py --input ./load/medic/32_val/11444.png --output ./../single_image_demo/output/Medical_output_places.png --model ./save/_train_Places365-32-256_liif/epoch-100.pth   --gpu 0 --resolution 256,256
echo "Places done"
python -W ignore demo.py --input ./load/medic/32_val/11444.png --output ./../single_image_demo/output/Medical_output_div2k.png --model ./../pretrained_models/edsr-baseline-liif.pth  --gpu 0 --resolution 256,256
echo "DIv2k done"
python -W ignore demo.py --input ./load/medic/32_val/11444.png --output ./../single_image_demo/output/Medical_output_medical.png --model ./save/_train-medical/epoch-200.pth   --gpu 0 --resolution 256,256
echo "Medical done"
end=$(date +%s)
echo "Elapsed Time: $(($end-$start)) seconds"
echo "script END"