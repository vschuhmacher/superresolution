#!/bin/bash

#SBATCH --output=../log/Test_Places365_%j.out
#SBATCH --gres=gpu:1
#SBATCH --mem=30G

echo "script START"

source /usr/itetnas04/data-scratch-01/$USER/data/conda/etc/profile.d/conda.sh
conda activate liif_v4
echo "Conda Environment Activated"
#
#./../pretrained_models/edsr-baseline-liif.pth


start=$(date +%s)
#python -u scripts/hello.py "$@"
python -W ignore demo.py --input ./load/div2k/DIV2K_valid_LR_bicubic/X4/0801x4.png --output ./../single_image_demo/output/Pingu.png --model ./../pretrained_models/edsr-baseline-liif.pth   --gpu 0 --resolution 1356,2040
#python -W ignore demo.py --input ./load/Places365/32/00025.png --output ./../single_image_demo/output/Places_output_100_chen.png --model ./../pretrained_models/edsr-baseline-liif.pth  --gpu 0 --resolution 3200,3200
#python -W ignore demo.py --input ./load/Places365/32/00025.png --output ./../single_image_demo/output/Places_output_100_medic.png --model ./save/_train-medical/epoch-200.pth  --gpu 0 --resolution 3200,3200

end=$(date +%s)
echo "Elapsed Time: $(($end-$start)) seconds"
echo "script END"