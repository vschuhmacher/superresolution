#!/bin/bash

#SBATCH --output=../log/Test_Places365_%j.out
#SBATCH --gres=gpu:1
#SBATCH --mem=30G

echo "script START"

source /usr/itetnas04/data-scratch-01/$USER/data/conda/etc/profile.d/conda.sh
conda activate liif_v4
echo "Conda Environment Activated"


start=$(date +%s)
#python -u scripts/hello.py "$@"
python test.py --config ./configs/test/test-Places365.yaml --model ./save/_train_Places365-32-256_liif/epoch-100.pth --gpu 0


end=$(date +%s)
echo "Elapsed Time: $(($end-$start)) seconds"
echo "script END"