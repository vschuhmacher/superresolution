#!/bin/bash

#SBATCH --output=../log/Train_Places365_%j.out
#SBATCH --gres=gpu:1
#SBATCH --mem=30G

echo "script START"

source /usr/itetnas04/data-scratch-01/$USER/data/conda/etc/profile.d/conda.sh
conda activate liif_v4
echo "Conda Environment Activated"


start=$(date +%s)
#python -u scripts/hello.py "$@"
python train_liif.py --config ./configs/train-Places365/train_Places365-32-256_liif.yaml


end=$(date +%s)
echo "Elapsed Time: $(($end-$start)) seconds"
echo "script END"