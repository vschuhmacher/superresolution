import os
from PIL import Image
from tqdm import tqdm

for size in [256, 128, 64, 32]:
    if size == 256:
        inp = './medic_images_val'
    else:
        inp = './256_val'
    print(size)
    os.mkdir(str(size)+'_val')
    filenames = os.listdir(inp)
    for filename in tqdm(filenames):
        Image.open(os.path.join(inp, filename)) \
            .resize((size, size), Image.BICUBIC) \
            .save(os.path.join('.', str(size)+'_val', filename.split('.')[0] + '.png'))
