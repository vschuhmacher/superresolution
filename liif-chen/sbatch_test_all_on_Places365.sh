#!/bin/bash

#SBATCH --output=../log/Test_all_Places_x2%j.out
#SBATCH --gres=gpu:1
#SBATCH --mem=30G

echo "script START"

source /usr/itetnas04/data-scratch-01/$USER/data/conda/etc/profile.d/conda.sh
conda activate liif_v4
echo "Conda Environment Activated"


start=$(date +%s)
#python -u scripts/hello.py "$@"
echo "Medical on Places"
python -W ignore test.py --config ./configs/test/test-Places365.yaml --model ./save/_train-medical/epoch-200.pth --gpu 0
echo "Div on Places"
python -W ignore test.py --config ./configs/test/test-Places365.yaml --model ../pretrained_models/edsr-baseline-liif.pth --gpu 0
echo "Places on Places365"
python -W ignore test.py --config ./configs/test/test-Places365.yaml --model ./save/_train_Places365-32-256_liif/epoch-100.pth --gpu 0



end=$(date +%s)
echo "Elapsed Time: $(($end-$start)) seconds"
echo "script END"