#!/bin/bash

#SBATCH --output=../log/Test_Div2k_%j.out
#SBATCH --gres=gpu:1
#SBATCH --mem=30G

echo "script START"

source /usr/itetnas04/data-scratch-01/$USER/data/conda/etc/profile.d/conda.sh
conda activate liif_v4
echo "Conda Environment Activated"


start=$(date +%s)
#python -u scripts/hello.py "$@"
bash ./scripts/test-div2k.sh ../pretrained_models/rdn-liif.pth 0


end=$(date +%s)
echo "Elapsed Time: $(($end-$start)) seconds"
echo "script END"