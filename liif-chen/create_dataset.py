from torchvision import datasets
from torchvision.transforms import ToPILImage
import torch
from PIL import Image
import os
from tqdm import tqdm

dataset_folder = './load/Places365'
if not os.path.exists(dataset_folder):
    os.makedirs(dataset_folder, exist_ok=True)
    print("Dataset folder created!")

topil = ToPILImage()
dataset = datasets.Places365(root= dataset_folder,split='train-standard', download= False) #IMPORTANT: once the dataset is downloaded set download= False to modify the data!
print("Downloaded Dataset of length: ",len(dataset))
print(dataset[0][0])

extracted_dataset = './load/Places365/data1024x1024'
if not os.path.exists(extracted_dataset):
    os.makedirs(extracted_dataset, exist_ok=True)
    print("Dataset folder created!")

for i in tqdm(range(30001)):#10000 Images should suffice for training? Else convert len(dataset) into one folder with png images
    image,label = dataset[60*i]
    # image_pil = topil(image)
    image.save(f'./load/Places365/data1024x1024/{i+1:05d}.png', format="png")