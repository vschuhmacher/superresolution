import h5py
import numpy as np
import os
import matplotlib.pyplot as plt
from PIL import Image


file1 = h5py.File('./load/data_T1_2d_size_256_256_depth_256_res_0.7_0.7_from_0_to_20.hdf5', 'r')
file2 = h5py.File('./load/data_T1_2d_size_256_256_depth_256_res_0.7_0.7_from_20_to_25.hdf5', 'r')
file3= h5py.File('./load/data_T1_2d_size_256_256_depth_256_res_0.7_0.7_from_50_to_70.hdf5', 'r')

#print(list(hdf5_file.keys()))

image_dataset = file1['images']
image_dataset2 =file2['images']
image_dataset3 = file3['images']
#print(Image.fromarray(image_dataset[0]))


extracted_dataset = './load/medic/medic_images_val'
if not os.path.exists(extracted_dataset):
    os.makedirs(extracted_dataset, exist_ok=True)
    print("Dataset folder created!")
extracted_dataset = './load/medic/medic_images_train'
if not os.path.exists(extracted_dataset):
    os.makedirs(extracted_dataset, exist_ok=True)
    print("Dataset folder created!")

extracted_dataset = './load/medic/test'
if not os.path.exists(extracted_dataset):
    os.makedirs(extracted_dataset, exist_ok=True)
    print("Dataset folder created!")

print(len(image_dataset) + len(image_dataset2) + len(image_dataset3))
print(len(image_dataset2))
print(np.array(image_dataset[99]))


for i in range(5):
    image = np.array(image_dataset[i])
    size = np.zeros((3, 256, 256))
    size[0] = image
    plt.imsave(f'./load/medic/test/{i+1:05d}.png', image)
    img






# for k in range(len(image_dataset3)):
#     i=i+1
#     image = image_dataset3[k]
 #    plt.imsave(f'./load/medic/medic_images_train/{i+1:05d}.png', image)

# for u in range(len(image_dataset2)):
#     i=i+1
#     image = image_dataset2[u]
#     plt.imsave(f'./load/medic/medic_images_val/{i+1:05d}.png', image)


file1.close()
file2.close()
file3.close()