#!/bin/bash

#SBATCH --output=../log/Test_Prior_%j.out
#SBATCH --gres=gpu:1
#SBATCH --mem=30G

echo "script START"

source /usr/itetnas04/data-scratch-01/$USER/data/conda/etc/profile.d/conda.sh
conda activate liif_v4
echo "Conda Environment Activated"


start=$(date +%s)
#python -u scripts/hello.py "$@"
for i in {50..2000..50}
do
    python -W ignore demo.py --input ./load/div2k/single_image/low_res/X4/0801x4.png --output ./../single_image_demo/output/all_epochs/Prior_test_v_3_5_$i.png --model ./save/_train_deep_image_prior_v3_5/epoch-$i.pth   --gpu 0 --resolution 1356,2040
done
end=$(date +%s)
echo "Elapsed Time: $(($end-$start)) seconds"
echo "script END"