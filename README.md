# LIIF

This repository contains the implementations for our project in the P&S course 'Deep Learining for Image Manipulaiton' at ETH Zürich in the fall semester of 2023. The basis for our project was:

[**Learning Continuous Image Representation with Local Implicit Image Function**](https://arxiv.org/abs/2012.09161)
<br>
[Yinbo Chen](https://yinboc.github.io/), [Sifei Liu](https://www.sifeiliu.net/), [Xiaolong Wang](https://xiaolonw.github.io/)
<br>
CVPR 2021 (Oral)

The original project page with video is at https://yinboc.github.io/liif/.

This is a Project by: Nicolas Dickenmann, Neil Reichlin, Viturin Schuhmacher and Pascal Wesemann, supervised by Ertunç Erdil.

---

In the following, we guide you through the steps to reconstruct our experiments and results.


### Environment
- Python 3
- Pytorch 1.6.0
- TensorboardX
- yaml, numpy, tqdm, imageio

install conda `liif_v4` enviroment:
`conda env create -f liif_conda_env.yml


Further information in Documentation/installation.md

## Quick Start with the trained models
1. Convert your image to LIIF and present it in a given resolution (with GPU 0, `[MODEL_PATH]` denotes the `.pth` file)

```
python demo.py --input xxx.png --model [MODEL_PATH] --resolution [HEIGHT],[WIDTH] --output output.png --gpu 0
```
2. You may also use one of our already implemented sbatch files for a quick start

## Reproducing Experiments

### Data



- **DIV2K, Benchmark datasets, Medical dataset**: Already in the git in liif-chen/load

- **Places365 dataset**: under liif-chen/create_dataset.py (set download=True, ATTENTION it is not recommended to download this dataset since it contains a large number of images)



### Running the code

**0. Preliminaries**

- For `train_liif.py` or `test.py`, use `--gpu [GPU]` to specify the GPUs (e.g. `--gpu 0` or `--gpu 0,1`).

- For `train_liif.py`, by default, the save folder is at `save/_[CONFIG_NAME]`. We can use `--name` to specify a name if needed.

- For dataset args in configs, `cache: in_memory` denotes pre-loading into memory (may require large memory, e.g. ~40GB for DIV2K), `cache: bin` denotes creating binary files (in a sibling folder) for the first time, `cache: none` denotes direct loading. We can modify it according to the hardware resources before running the training scripts.

**1. Experiments**

**Train**: `python train_liif.py --config configs/train-div2k/train_edsr-baseline-liif.yaml` (with EDSR-baseline backbone, for RDN replace `edsr-baseline` with `rdn`). We use 1 GPU for training EDSR-baseline-LIIF and 4 GPUs for RDN-LIIF.

**Test**: `bash scripts/test-div2k.sh [MODEL_PATH] [GPU]` for div2k validation set, `bash scripts/test-benchmark.sh [MODEL_PATH] [GPU]` for benchmark datasets. `[MODEL_PATH]` is the path to a `.pth` file, we use `epoch-last.pth` in corresponding save folder.

**Our tests:**
All our tests can be run from the corresponding shell scripts in `liif_chen/`, further information under `Documentation/How_to_Test.md`

## LIIF for encoding Medical images
This more sepreated part of the Project was also inpired by [**Implicit Neural Representations for Image Compression**](https://arxiv.org/abs/2112.04267). The goal was to encode 2D and 3D images by overfitting into the weights of the LIIF. To reproduce the LIIF is sampled.

All files regarding this part are locaed int the `medical_liif` folder. The 2D and 3D models are identical expect of the batch size and the number of nodes in the linear layers. Additionally ther is each a `.jpynb` file to visualize the results, and a `.py` file, sutch that the training can be done on the slurm gpu cluster. A files in this folder were run with th `medical1` enviroment locaed in the folder. Alltough it should also work with the `liif_v4` enviroment

**Trained models:** 
In the `output_intermediate_model_2D` folder, there are the models of one slice. Whenever the loss was lower than previosly a model was saved.\
`model_20231211_083725_$EPOCH` is trained with 256 nodes in one linear layer. \
`model_20231213_162529_E$EPOCH_L$LOSS` is trained with 512 nodes in one linear layer.

In the `output_intermediate_model_3D` folder, there are the models of one 3D volume. Whenever the loss was lower than previosly a model was saved. Unfortunately, the training aborted and it only trained 64 Epochs. While inspecting the result, no training effect at all was found. But before continuing the training for 3D Images, the LEIF need to be improved. As it wasn't even in the 2D case, not able to represent the image fully. 


`20231214_191521_S$SCAN_E$EPOCH_L$LOSS` is trained with 1024 nodes in one linear layer. 







## Journal, Report and further help

Further information on our work in Documentation/Report and Documentation/journal.md
