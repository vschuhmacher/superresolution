# Journal
## 25.10
### Viturin
First test of running single images. Fazit: The Jepg arthefacts get upscaled as well, and are well visible.
### Neil
1. Conda setup, some problems with installation path and netscratch directory.
2. Creating yml file to share conda environment.

## 28.10
### Viturin
1. Clean set up of gitlab. clone of the original liif repo.
2. Seting up ssh access from tardis account to gitlab, Writing installation.md for documentation.
3. Testing new setup for test images.

## 31.10
### Viturin
1. Setting up documentation
### Neil
1. Running tests on the benchmark dataset, confirmed findings
2. Failed to run div2k tests because of memory issues


## 31.10
### Nicolas
1. Searching for dataset
2. Setting up test with 5 GPUs

## 1.11
### Nicolas 
1. squeue -u <username> lists your uncompleted jobs
### Pascal
1. successful ssh connection to server, everything set up (finally)

## 2.11
### Neil 
1. Tried downloading celeba with torchvision.dataset but it could not download automatically from googgle drive
2. Tried downloading celaba manually but there were too many files and also they were low resolution images
3. Tried downloading places 365 but download got corrupted once.
4. Downloading places365 and also wrote a script to extract some images from it and store it in a way that their resize funcion can use it `create_dataset.py`

## 3.11
### Meeting with Ertunc
#### Creating Places365 Dataset:
1. Neil prepared 30'000 images to train & test on (currently not on git)
2. Nicolas wrote config file
#### Requesting 5 GPUs:
1. we may have a quota, try with 2, 3 or 4
#### Problem with not enough memory running div2k test:
1. to ignore warnings in log file execute code with `-W ignore` in command: `python -W ignore foo.py`
2. batch size is already set to 1
3. unfortunately there are no comments in the code
4. using debugging tool to get more information
5. debugging tool reveals memory usage of GPU seems to be ok until code crashes (throws known out of memory error)

### Nicolas
1. ajusted config files for Places365 dataset
2. wrote sbatch file for Places365 dataset

## 5.11.
### Neil
1. Prepared dataset for training and testing.
2. Trained for 100 epochs (4.5h)
3. Ran the model on some images and looks promising, with further training similiar performance i would say (Tests need to confirm)
4. Updated how to use new model
5. Wrote a test-script to test on single input images

## 6.11
### Neil
1. Tested the model trained for 100 epochs on the test-set of Places365
2. Created `test_matrix.md` to compare results

## 6.11
### Nicolas
1. Tested div2k on Places365
2. Set up midterm presentation