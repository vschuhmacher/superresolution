### Cross Testing on Multiple Domains

| Training  | Div2k-Test | Benchmark-Test | Places365-Test | Medical|
| --------- | ---------- | -------------- | -------------- | -------|
| Div2k     |            |                |                |
| Places365 |            |                | 22.8011        |
| Medical   |            |                |                |31.4165
|           |            |                |                |



Values are given in PSNR(dB) (see https://en.wikipedia.org/wiki/Peak_signal-to-noise_ratio)