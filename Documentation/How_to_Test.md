# How to run Tests

## Running the Div2k Tests

Running  `sbatch sbatch_run_experiments_div2k.sh` runs  `/liif-chen/scrips/test-div2k.sh` on the GPU-cluster.
The output is still written into `/superresolution/log`.
ATTENTION: is made to be run from within liif-chen

**Problems**

Some problems arise concerning Memory-usage, see `/superresolution/log/Memory_error.out/`.
Also in some later iterations problems with the file-structure in `/liif-chen/load/div2k` could arise but fix should be to create subfolders called `X2, X3, X4` under one single folder `DIV2k_valid_LR_bicubic` but the error doesn't arise because the execution never reaches the iteration for x3 and the file-path for x2 is correct.

To ignore warnings in log file execute code with `-W ignore` in command: `python -W ignore foo.py`


# How to create the dataset

I wrote a python script which we can load datasets and should be able to create a similar file-structure as the one which they provide or require to train on the celebAHQ dataset. This way we can just reuse the `resize.py` script.
Once the `dataset.Places365` is installed (you should not need to do this anymore) use the function with `download=False` to use the pytorch functionality and be able to index the dataset easily.
The selected files should be stored in a folder called `1024x1024` and then `resize_places.py` can be called within the folder to create the 32, 64, 128 and 256 square images.
Like this the dataset is ready for training.


# How to train the dataset

`sbatch_train_Places365.sh` runs the training script and stores intermediate results (models) inside `/save`.
When adding a new line inside the Places-Config-file : `resume: *.pth` a previously trained model can be trained further!
