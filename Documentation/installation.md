# installation
## 1. Connect to a tardis.
__SSH into tardis with VS-Code__

See DILM slids

at home use ETH-VPN

* Room: ETZ/D/61.1	 Computers: tardis-a01 - 38
* Room: ETZ/D/96.1 	 Computers: tardis-b01 - 37
* Room: ETZ/D/61.2 	 Computers: tardis-d01 - 20
* Room: ETF/B/5		 Computers: tardis-c01 - 21

You can connect remotely to the computers using:

`ssh [username]@tardis-AXY.ee.ethz.ch`
`


## 2. install anaconda
see DILM slids, use script. 

install into: ` /usr/itetnas04/data-scratch-01/$USER/data`


## 3. Setup ssh with gitlab
in tardis home directory Terminal run:

```ssh-keygen -t ed25519 -C "DLMI-Gitlab"```

display created Public (__.pub !!__) key and copy __all__ contet of the file. use `ls` or tab to fill the xxxxx. 

`cat .ssh/id_edxxxxx.pub`

Go to `https://gitlab.ethz.ch/-/profile/key` and click add new key.

Paste __all__ content of the file in to the Key field. Edit nothing else and click Add Key.

## 4. get into the data directory
Back in the VS-Code terminal: Browse to the Data directory with `cd /usr/itetnas04/data-scratch-01/$USER/data`

(you can leave `$USER` as it is. It's a bash variable)

type: `code .` with "SPACE" and "."  and a new VS-code windows opens. 

## 5. Clone repository
run

`git clone git@gitlab.ethz.ch:vschuhmacher/superresolution.git`

## 6. install conda enviroment
change to superresulotion directory: 
`cd superresolution`

install conda `liif_v4` enviroment:
`conda env create -f liif_conda_env.yml`

## 7. Set up GPU cluster
See DLIM slides

Get an interactive session for debugging: 
`srun --time 10 --gres=gpu:1 --pty bash -i`

Print the GPU information: 
`nvidia-smi``


## 8 Test GPU cluster and liif
modify the `sbatch_run.sh` file to selct the python programm you wish running. 

int the main superreulotion folder, run:
`sbatch sbatch_run.sh`

check the result in the log folder


## 9 using git
in the superresulotion directory:

* git pull
* git add *
* git commit -m "your commit message"
* git push







