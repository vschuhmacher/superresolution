#!/bin/bash

#SBATCH --output=log/2Dmedical_liif_%j.out
#SBATCH --gres=gpu:2
#SBATCH --mem=30G

echo "script START"

source /usr/itetnas04/data-scratch-01/$USER/data/conda/etc/profile.d/conda.sh
conda activate medical1
echo "Conda Environment Activated"


start=$(date +%s)
echo "start py"
python '10_2Dmedical_script.py'


end=$(date +%s)
echo "Elapsed Time: $(($end-$start)) seconds"
echo "script END"