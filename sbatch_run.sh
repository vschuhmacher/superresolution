#!/bin/bash

#SBATCH --output=log/%j.out
#SBATCH --gres=gpu:1
#SBATCH --mem=30G

echo "script START"

source /usr/itetnas04/data-scratch-01/$USER/data/conda/etc/profile.d/conda.sh
conda activate liif_v4
echo "Conda Environment Activated"


start=$(date +%s)
#python -u scripts/hello.py "$@"
python liif-chen/demo.py --input single_image_demo/images/castle.png --model pretrained_models/edsr-baseline-liif.pth  --resolution 3000,3000 --output single_image_demo/output/castle_out1.png --gpu 0
python -u scripts/cuda_test.py
end=$(date +%s)
echo "Elapsed Time: $(($end-$start)) seconds"

echo "script END"